
#ifndef otbDomainAdaptationMAD_hxx
#define otbDomainAdaptationMAD_hxx

#include "otbDomainAdaptationMAD.h"
#include "itkProgressReporter.h"
namespace otb
{
template<class TInputImage, class TOutputImage>
DomainAdaptationMADFilter<TInputImage, TOutputImage>
::DomainAdaptationMADFilter()
  :  m_BackgroundValue(-10000)
{
  this->SetNumberOfRequiredInputs(1);
}

template<class TInputImage, class TOutputImage>
void DomainAdaptationMADFilter<TInputImage, TOutputImage>
::SetInput(const TInputImage * image)
{
  this->SetNthInput(0, const_cast<TInputImage *>(image));
}

template<class TInputImage, class TOutputImage>
const typename DomainAdaptationMADFilter<TInputImage, TOutputImage>
::InputImageType * DomainAdaptationMADFilter<TInputImage, TOutputImage>
::GetInput()
{
  if (this->GetNumberOfInputs() < 1)
    {
      return nullptr;
    }
  return static_cast<const TInputImage *>(this->itk::ProcessObject::GetInput(0));
}

template <class TInputImage, class TOutputImage>
void
DomainAdaptationMADFilter<TInputImage, TOutputImage>
::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId)
{
  const TInputImage * inputPtr = this->GetInput();
  TOutputImage * outputPtr = this->GetOutput();
  
  typedef itk::ImageRegionConstIterator<InputImageType>  ConstIteratorType;
  typedef itk::ImageRegionIterator<OutputImageType> IteratorType;

  IteratorType outIt(outputPtr, outputRegionForThread);
  ConstIteratorType inIt(inputPtr, outputRegionForThread);

  inIt.GoToBegin();
  outIt.GoToBegin();

  unsigned int nbComp = inputPtr->GetNumberOfComponentsPerPixel();

  // Compute the number of input band according to the correlation values
  // A correlation lower than 0.001 means the time series have not the same dimensions

  // TODO: warning if 0<Rho<1 not respected
  // unsigned int nbOutComp = 0;
  // for (unsigned int i=0; i< m_Rho.size(); ++i)
  //   {
  //     if(m_Rho[i] > m_MinCorr)
  // 	{
  // 	  nbOutComp+= 1;
  // 	}
  //   }
  // std::cout << "nb out comp" << m_NbOutBands << std::endl;
  assert((nbOutComp>nbComp)&&"error the number of requested band cannot be larger than the number of the input image. Verify the MinCorr parameter value.");
  outputPtr->SetNumberOfComponentsPerPixel(m_NbOutBands);

  itk::ProgressReporter progress(this, threadId, outputRegionForThread.GetNumberOfPixels());

  while(!inIt.IsAtEnd() && !outIt.IsAtEnd())
    {
      // Init output pixel
      typename OutputImageType::PixelType outPixel(m_NbOutBands);
      
      // if (inIt.Get()[0] == m_BackgroundValue)
      // 	{
      // 	  for(unsigned int i = 0; i< nbOutComp; ++i)
      // 	    {
      // 	      outPixel[i] = m_BackgroundValue;
      // 	    }
      // 	  outIt.Set(outPixel);
      // 	  continue;
      // 	}
      // Put the pixel values in a vnlvector
      VnlVectorType x1(nbComp, 0);
      for(unsigned int i = 0; i < nbComp; ++i)
	{
	  x1[i] = inIt.Get()[i];
	}
      // Project the data
      VnlVectorType first = (x1-m_Mean)*m_V;
      // Dimension reduction
      if (m_SameNumberOfBands)
	{
	  
	  for(unsigned int i=0; i< m_NbOutBands; ++i)
	    {
	      outPixel[i] = first[nbComp - i];
	    } 
	}
      else
	{
	  for(unsigned int i=0; i< m_NbOutBands; ++i)
	    {
	      outPixel[i] = first[i];
	    }
	}
      outIt.Set(outPixel);
      if (inIt.Get()[0] == m_BackgroundValue )
	{
	  outPixel.Fill(m_BackgroundValue);
	  outIt.Set(outPixel);
	}
      ++inIt;
      ++outIt;
      progress.CompletedPixel();
    }
}
} // end namespace otb  
#endif
