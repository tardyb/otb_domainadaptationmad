#ifndef _COMPUTECHANGEPROBABILITIESFILTER_H_
#define _COMPUTECHANGEPROBABILITIESFILTER_H_
#include "vnl/vnl_vector.h"
namespace otb
{

template<class TInputImage, class TOutputImage>
class ITK_EXPORT ComputeChangeProbabilitiesFilter
  : public itk::ImageToImageFilter<TInputImage, TOutputImage>
{
 public:
  /** Standard class typedefs. */
  typedef ComputeChangeProbabilitiesFilter                   Self;
  typedef itk::ImageToImageFilter<TInputImage, TOutputImage> Superclass;
  typedef itk::SmartPointer<Self>                            Pointer;
  typedef itk::SmartPointer<const Self>                      ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Run-time type information (and related methods). */
  itkTypeMacro(DomainAdaptationMADFilter, ImageToImageFilter);

  /** Some convenient typedefs. */
  typedef TInputImage                            InputImageType;
  typedef typename InputImageType::Pointer      InputImagePointer;
  typedef typename InputImageType::ConstPointer InputImageConstPointer;
  typedef typename InputImageType::RegionType   InputImageRegionType;
  typedef typename InputImageType::PixelType     InputImagePixelType;
  typedef TOutputImage                           OutputImageType;
  typedef typename OutputImageType::Pointer      OutputImagePointer;
  typedef typename OutputImageType::RegionType   OutputImageRegionType;
  typedef typename OutputImageType::PixelType    OutputImagePixelType;

  typedef vnl_vector<double>                                 VnlVectorType;
  /* Set and Get Macro */
  itkSetMacro(BackgroundValue, double);
  itkSetMacro(Rho, VnlVectorType);
  //void SetInput(const TInputImage * image);
  const TInputImage * GetInput();
 protected:
  ComputeChangeProbabilitiesFilter();
  ~ComputeChangeProbabilitiesFilter() override {}

  void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId) override;

  void GenerateOutputInformation ()
  {
    Superclass::GenerateOutputInformation();
    this->GetOutput()->SetNumberOfComponentsPerPixel(1);
  }

 private:
  double m_BackgroundValue;
  VnlVectorType m_Rho;
};
  
  
} // end namespace otb


#ifndef OTB_MANUAL_INSTANTIATION
#include "otbComputeChangeProbabilitiesFilter.hxx"
#endif
#endif
