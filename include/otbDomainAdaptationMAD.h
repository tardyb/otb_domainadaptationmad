#ifndef _DOMAINADAPTATIONMAD_H_
#define _DOMAINADAPTATIONMAD_H_

// #include "otbMultivariateAlterationDetectorImageFilter.h"
#include "vnl/vnl_vector.h"
#include "vnl/vnl_matrix.h"

namespace otb
{

  /**
   * This filter project the input image in the space defined
   * by the MAD algorithms eigenvectors.
   */
template<class TInputImage, class TOutputImage>
class ITK_EXPORT DomainAdaptationMADFilter
  : public itk::ImageToImageFilter<TInputImage, TOutputImage>
{
 public:
  /** Standard class typedefs. */
  typedef DomainAdaptationMADFilter           Self;
  typedef itk::ImageToImageFilter<TInputImage, TOutputImage>  Superclass;
  typedef itk::SmartPointer<Self>                             Pointer;
  typedef itk::SmartPointer<const Self>                       ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Run-time type information (and related methods). */
  itkTypeMacro(DomainAdaptationMADFilter, ImageToImageFilter);

  /** Some convenient typedefs. */
  typedef TInputImage                            InputImageType;
  typedef typename InputImageType::Pointer      InputImagePointer;
  typedef typename InputImageType::ConstPointer InputImageConstPointer;
  typedef typename InputImageType::RegionType   InputImageRegionType;
  typedef typename InputImageType::PixelType     InputImagePixelType;
  typedef TOutputImage                           OutputImageType;
  typedef typename OutputImageType::Pointer      OutputImagePointer;
  typedef typename OutputImageType::RegionType   OutputImageRegionType;
  typedef typename OutputImageType::PixelType    OutputImagePixelType;

  typedef vnl_vector<double>                                 VnlVectorType;
  typedef vnl_matrix<double>                                 VnlMatrixType;

  /* Set the mean of bands for image */
  itkSetMacro(Mean, VnlVectorType);
  itkSetMacro(V, VnlMatrixType);
  itkSetMacro(Rho, VnlVectorType);
  itkSetMacro(SameNumberOfBands, bool);
  itkSetMacro(NbOutBands, double);
  itkSetMacro(BackgroundValue, double);
  void SetInput(const TInputImage * image);
  const TInputImage * GetInput();
 protected:
  DomainAdaptationMADFilter();
  ~DomainAdaptationMADFilter() override {}

  void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId) override;

  void GenerateOutputInformation ()
  {
    Superclass::GenerateOutputInformation();
    this->GetOutput()->SetNumberOfComponentsPerPixel(m_NbOutBands);
  }
 private:
  VnlMatrixType m_V;
  VnlVectorType m_Mean;
  VnlVectorType m_Rho;
  double m_NbOutBands;
  double m_BackgroundValue;
  bool m_SameNumberOfBands;
};
  
} // end namespace otb
#ifndef OTB_MANUAL_INSTANTIATION
#include "otbDomainAdaptationMAD.hxx"
#endif
#endif
