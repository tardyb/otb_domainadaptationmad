#ifndef _MASKIMAGEFILTER_HXX
#define _MASKIMAGEFILTER_HXX

#include "otbMaskImageFilter.h"

namespace otb
{
template <class TInputImage, class TOutputImage>
void
MaskImageFilter<TInputImage, TOutputImage>
::SetInput1(const TInputImage * image1)
{
  // Process object is not const-correct so the const casting is required.
  this->SetNthInput(0, const_cast<TInputImage *>(image1));
}

template <class TInputImage, class TOutputImage>
const typename MaskImageFilter<TInputImage, TOutputImage>
::InputImageType *
MaskImageFilter<TInputImage, TOutputImage>
::GetInput1()
{
  if (this->GetNumberOfInputs() < 1)
    {
    return nullptr;
    }
  return static_cast<const TInputImage *>(this->itk::ProcessObject::GetInput(0));
}

template <class TInputImage, class TOutputImage>
void
MaskImageFilter<TInputImage, TOutputImage>
::SetInput2(const TInputImage * image2)
{
  // Process object is not const-correct so the const casting is required.
  this->SetNthInput(1, const_cast<TInputImage *>(image2));
}

template <class TInputImage, class TOutputImage>
const typename MaskImageFilter<TInputImage, TOutputImage>
::InputImageType *
MaskImageFilter<TInputImage, TOutputImage>
::GetInput2()
{
  if (this->GetNumberOfInputs() < 2)
    {
    return nullptr;
    }
  return static_cast<const TInputImage *>(this->itk::ProcessObject::GetInput(1));
}

template <class TInputImage, class TOutputImage>
void MaskImageFilter<TInputImage,TOutputImage>
::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId)
{
  
  // Retrieve input images pointers
  // Image
  const TInputImage * input1Ptr = this->GetInput1();
  // mask
  const TInputImage * input2Ptr = this->GetInput2();
  TOutputImage * outputPtr = this->GetOutput();

  typedef itk::ImageRegionConstIterator<InputImageType>  ConstIteratorType;
  typedef itk::ImageRegionIterator<OutputImageType> IteratorType;

  IteratorType outIt(outputPtr, outputRegionForThread);
  ConstIteratorType inIt1(input1Ptr, outputRegionForThread);
  ConstIteratorType inIt2(input2Ptr, outputRegionForThread);

  inIt1.GoToBegin();
  inIt2.GoToBegin();
  outIt.GoToBegin();

  assert(input2Ptr->GetNumberOfComponentsPerPixel()==1&&"Error: mask must be one band binary image.");

  itk::ProgressReporter progress(this, threadId, outputRegionForThread.GetNumberOfPixels());
  unsigned int outNbComp = outputPtr->GetNumberOfComponentsPerPixel();
  while(!inIt1.IsAtEnd() && !inIt2.IsAtEnd() && !outIt.IsAtEnd())
    {
      typename OutputImageType::PixelType outPixel(outNbComp);
      if(inIt2.Get()[0] == 0)
	{
	  outPixel.Fill(m_BackgroundValue);
	  outIt.Set(outPixel);
	}
      else
	{
	  outIt.Set(inIt1.Get());
	}

    }
}
  
} //namespace otb
#endif
