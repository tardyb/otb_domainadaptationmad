#ifndef _MASKIMAGEFILTER_H
#define _MASKIMAGEFILTER_H

namespace otb
{

  /**
   * This filter mask each band of the input image.
   * Where the mask value is 0 the filter set a no data value in 
   * the output image.
   */
template<class TInputImage, class TOutputImage>
class ITK_EXPORT MaskImageFilter
  : public itk::ImageToImageFilter<TInputImage, TOutputImage>
{
 public:
  /** Standard class typedefs. */
  typedef MaskImageFilter           Self;
  typedef itk::ImageToImageFilter<TInputImage, TOutputImage>  Superclass;
  typedef itk::SmartPointer<Self>                             Pointer;
  typedef itk::SmartPointer<const Self>                       ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Run-time type information (and related methods). */
  itkTypeMacro(MaskImageFilter, ImageToImageFilter);

  /** Some convenient typedefs. */
  typedef TInputImage                            InputImageType;
  typedef typename InputImageType::Pointer      InputImagePointer;
  typedef typename InputImageType::ConstPointer InputImageConstPointer;
  typedef typename InputImageType::RegionType   InputImageRegionType;
  typedef typename InputImageType::PixelType     InputImagePixelType;
  typedef typename InputImageType::InternalPixelType ValueType;
  typedef TOutputImage                           OutputImageType;
  typedef typename OutputImageType::Pointer      OutputImagePointer;
  typedef typename OutputImageType::RegionType   OutputImageRegionType;
  typedef typename OutputImageType::PixelType    OutputImagePixelType;

  /* SET GET MACRO */
  itkSetMacro(BackgroundValue, ValueType);


  /** Connect one of the operands for pixel-wise addition */
  void SetInput1(const TInputImage * image1);

  /** Connect one of the operands for pixel-wise addition */
  void SetInput2(const TInputImage * image2);

  /** Get the inputs */
  const TInputImage * GetInput1();
  const TInputImage * GetInput2();


 protected:
  MaskImageFilter() {}
  ~MaskImageFilter() override {}

  void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId) override;

  //void GenerateOutputInformation() override;
 private:
  ValueType m_BackgroundValue;
  
}; //end of filter class
} //end namespace otb
#ifndef OTB_MANUAL_INSTANTIATION
#include "otbMaskImageFilter.hxx"
#endif
#endif
