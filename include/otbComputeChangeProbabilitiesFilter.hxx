#ifndef otbComputeChangeProbabilitiesFilter_hxx
#define otbComputeChangeProbabilitiesFilter_hxx

#include <boost/math/special_functions/gamma.hpp>

namespace otb
{
  
template<class TInputImage, class TOutputImage>
ComputeChangeProbabilitiesFilter<TInputImage, TOutputImage>
::ComputeChangeProbabilitiesFilter()
  : m_BackgroundValue(-10000)
{
  this->SetNumberOfRequiredInputs(1);
}

// template<class TInputImage, class TOutputImage>
// void ComputeChangeProbabilitiesFilter<TInputImage, TOutputImage>
// ::SetInput(const TInputImage * image)
// {
//   this->SetNthInput(0, const_cast<TInputImage *>(image));
//   std::cout << "input done\n";
// }

template<class TInputImage, class TOutputImage>
const typename ComputeChangeProbabilitiesFilter<TInputImage, TOutputImage>
::InputImageType * ComputeChangeProbabilitiesFilter<TInputImage, TOutputImage>
::GetInput()
{
  if (this->GetNumberOfInputs() < 1)
    {
      return nullptr;
    }
  return static_cast<const TInputImage *>(this->itk::ProcessObject::GetInput(0));
}

template <class TInputImage, class TOutputImage>
void
ComputeChangeProbabilitiesFilter<TInputImage, TOutputImage>
::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId)
{
  //std::cout << "in thread\n";

  const TInputImage * inputPtr = this->GetInput();
  TOutputImage * outputPtr = this->GetOutput();

  typedef itk::ImageRegionConstIterator<InputImageType> ConstIteratorType;
  typedef itk::ImageRegionIterator<OutputImageType> IteratorType;

  IteratorType outIt(outputPtr, outputRegionForThread);
  ConstIteratorType inIt(inputPtr, outputRegionForThread);

  inIt.GoToBegin();
  outIt.GoToBegin();

  unsigned int nbComp = inputPtr->GetNumberOfComponentsPerPixel();
  
  
  itk::ProgressReporter progress(this, threadId, outputRegionForThread.GetNumberOfPixels());

  while(!inIt.IsAtEnd() && !outIt.IsAtEnd())
    {
      //Init output pixel
      typename OutputImageType::PixelType outPixel(1);
      
      if (inIt.Get()[0] == m_BackgroundValue)
	{
	  // inutile le MAD ne renvoit pas de non data...
	  outPixel[0] = m_BackgroundValue;
	}
      else
	{
	  double chi2 = 0.0;
	  for (unsigned int i = 0; i < nbComp; i++)
	    {
	      double val = inIt.Get()[i]/sqrt(2*(1-m_Rho[i])); 
	      chi2 += val*val;
	    }
	  outPixel[0] =1-boost::math::gamma_q(0.5*chi2, 0.5*nbComp) ;
	}
      outIt.Set(outPixel);
      ++inIt;
      ++outIt;
      progress.CompletedPixel();
    }
}
} //end namespace otb
#endif
