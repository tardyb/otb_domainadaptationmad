#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
// Change detection filter
#include "otbMultivariateAlterationDetectorImageFilter.h"
// Rescaler header
#include "otbStatisticsXMLFileReader.h"
#include "otbShiftScaleVectorImageFilter.h"
// Mask header
#include "itkMaskImageFilter.h"
#include "otbImageToNoDataMaskFilter.h"
#include "otbChangeNoDataValueFilter.h"
#include "otbVectorImageToImageListFilter.h"
#include "otbImageListToVectorImageFilter.h"
#include "otbChangeInformationImageFilter.h"
// New filter for proba computing
#include "otbComputeChangeProbabilitiesFilter.h"

namespace otb
{
namespace Wrapper
{

class ComputeChangeProbabilities : public otb::Wrapper::Application
{

public:
  typedef ComputeChangeProbabilities    Self;
  typedef itk::SmartPointer<Self>       Pointer;

  itkNewMacro(Self);
  itkTypeMacro(ComputeChangeProbabilities, otb::Wrapper::Application);
  typedef otb::ImageList<FloatImageType> ImageListType;
  typedef itk::VariableLengthVector<FloatVectorImageType::InternalPixelType>                   MeasurementType;
  typedef otb::StatisticsXMLFileReader<MeasurementType>                                        StatisticsReader;
  typedef otb::ShiftScaleVectorImageFilter<FloatVectorImageType, FloatVectorImageType>         RescalerType;
  typedef otb::MultivariateAlterationDetectorImageFilter<FloatVectorImageType, FloatVectorImageType> ChangeFilterType;
  typedef itk::MaskImageFilter<FloatImageType, UInt8ImageType, FloatImageType> MaskFilterType;
  typedef otb::VectorImageToImageListFilter<FloatVectorImageType,ImageListType> VectorToListFilterType;
  typedef otb::ImageListToVectorImageFilter<ImageListType,FloatVectorImageType> ListToVectorFilterType;
  typedef otb::ChangeInformationImageFilter<FloatVectorImageType> ChangeInfoFilterType;

  typedef otb::ComputeChangeProbabilitiesFilter<FloatVectorImageType, FloatVectorImageType> ComputeProbaFilterType;
private:
  void DoInit()
  {
    SetName("ComputeChangeProbabilities");
    SetDescription("");

    AddParameter(ParameterType_InputImage, "in1", "Input Image 1");
    SetParameterDescription("in1", "");

    AddParameter(ParameterType_InputImage, "in2", "Input Image 2");
    SetParameterDescription("in2", "");

    AddParameter(ParameterType_OutputImage, "out", "The change  probabilities map");
    SetParameterDescription("out", "");

    AddParameter(ParameterType_Float, "bv", "Background Value");
    SetParameterDescription( "bv", "Background value to ignore in statistics computation. In the case of input mask is provided, the no data in the masked image are set to this background value and ignored." );
    SetDefaultParameterFloat("bv",0.0);
    MandatoryOff("bv");
    
    AddParameter(ParameterType_InputFilename, "imstat1", "Statistics file");
    SetParameterDescription("imstat1", "A XML file containing mean and standard deviation to center and reduce samples before classification (produced by ComputeImagesStatistics application).");
    MandatoryOff("imstat1");
    
    AddParameter(ParameterType_InputFilename, "imstat2", "Statistics file");
    SetParameterDescription("imstat2", "A XML file containing mean and standard deviation to center and reduce samples before classification (produced by ComputeImagesStatistics application).");
    MandatoryOff("imstat2");

    AddParameter(ParameterType_Choice,"mask","No-data handling mode");
    SetParameterDescription("mask","Set the different option for mask use and handling no data.");
    
    AddChoice("mask.bin", "use a binary mask");
    AddParameter(ParameterType_InputImage, "mask.bin.in", "Binary validity mask");
    SetParameterDescription("mask.bin.in", "");
    MandatoryOff("mask.bin.in");

    AddParameter(ParameterType_Float,"mask.bin.nodata", "The no data value in the input mask.");
    SetParameterDescription("mask.bin.nodata", "");
    SetDefaultParameterFloat("mask.bin.nodata",0.0);
    MandatoryOff("mask.bin.nodata");

    AddRAMParameter();
  }

  void DoUpdateParameters()
  {
    
  }

  void DoExecute()
  {
     // Filter declaration
    StatisticsReader::Pointer  statisticsReader = StatisticsReader::New();
    StatisticsReader::Pointer  statisticsReader2 = StatisticsReader::New();
    m_Rescaler = RescalerType::New();
    m_Rescaler2 = RescalerType::New();

    m_ChangeFilter = ChangeFilterType::New();
    m_ComputeProba = ComputeProbaFilterType::New();
    // usefull variables for log 
    MeasurementType  meanMeasurementVector;
    MeasurementType  stddevMeasurementVector;
    MeasurementType  meanMeasurementVector2;
    MeasurementType  stddevMeasurementVector2;

    // Set Background value
    m_Rescaler->SetIgnoreUserDefinedValue(true);
    m_Rescaler->SetUserIgnoredValue(GetParameterFloat("bv"));
    m_Rescaler2->SetIgnoreUserDefinedValue(true);
    m_Rescaler2->SetUserIgnoredValue(GetParameterFloat("bv"));

    m_ChangeFilter->SetIgnoreUserDefinedValue(true);
    m_ChangeFilter->SetUserIgnoredValue(GetParameterFloat("bv"));
    
    // Normalisation

    // Input 1
    otbAppLogINFO("Input image normalization activated for in1.");
    // Load input image statistics
    statisticsReader->SetFileName(GetParameterString("imstat1"));
    meanMeasurementVector   = statisticsReader->GetStatisticVectorByName("mean");
    stddevMeasurementVector = statisticsReader->GetStatisticVectorByName("stddev");
    otbAppLogINFO( "mean used: " << meanMeasurementVector );
    otbAppLogINFO( "standard deviation used: " << stddevMeasurementVector );
    // Set mean and var
    m_Rescaler->SetScale(stddevMeasurementVector);
    m_Rescaler->SetShift(meanMeasurementVector);
    // Rescale
    m_Rescaler->SetInput(GetParameterImage("in1"));
    m_Rescaler->GetOutput()->UpdateOutputInformation();

    // Input 2
    otbAppLogINFO("Input image normalization activated for in2.");
    // Load input image statistics
    statisticsReader2->SetFileName(GetParameterString("imstat2"));
    meanMeasurementVector2   = statisticsReader2->GetStatisticVectorByName("mean");
    stddevMeasurementVector2 = statisticsReader2->GetStatisticVectorByName("stddev");
    otbAppLogINFO( "mean used: " << meanMeasurementVector2 );
    otbAppLogINFO( "standard deviation used: " << stddevMeasurementVector2 );
    // Set mean and var
    m_Rescaler2->SetScale(stddevMeasurementVector2);
    m_Rescaler2->SetShift(meanMeasurementVector2);
    // Rescale
    m_Rescaler2->SetInput(GetParameterImage("in2"));
    m_Rescaler2->GetOutput()->UpdateOutputInformation();

     // Apply Mask 
    // UInt8ImageType::Pointer maskPtr = GetParameterImage<UInt8ImageType>("mask.bin.in");

    // Input 1
    //unsigned int nbBands1 = m_Rescaler->GetOutput()->GetNumberOfComponentsPerPixel();
    // sug: lire metadata depuis input1
    // itk::MetaDataDictionary &dict1 = m_Rescaler->GetOutput()->GetMetaDataDictionary();
    // std::vector<bool> flags1;
    // std::vector<double> values1;
    // bool ret1 = otb::ReadNoDataFlags(dict1, flags1, values1);
    // if(!ret1)
    //   {
    // 	flags1.resize(nbBands1, true);
    // 	values1.resize(nbBands1, GetParameterFloat("bv"));
    //   }
    // m_V2L1 = VectorToListFilterType::New();
    // m_V2L1->SetInput(m_Rescaler->GetOutput());
    // ImageListType::Pointer inputList1 = m_V2L1->GetOutput();
    // inputList1->UpdateOutputInformation();
    // ImageListType::Pointer outputList1 = ImageListType::New();
    // for(unsigned int i=0; i < nbBands1; ++i)
    //   {
    // 	if (flags1[i])
    // 	  {
    // 	    MaskFilterType::Pointer masker = MaskFilterType::New();
    // 	    masker->SetInput(inputList1->GetNthElement(i));
    // 	    masker->SetMaskImage(maskPtr);
    // 	    masker->SetMaskingValue(GetParameterFloat("mask.bin.nodata"));
    // 	    masker->SetOutsideValue(values1[i]);
    // 	    outputList1->PushBack(masker->GetOutput());
    // 	    m_MaskFilters1.push_back(masker);
    // 	  }
    // 	else
    // 	  {
    // 	    outputList1->PushBack(inputList1->GetNthElement(i));
    // 	  }
    //   }
    // m_MaskedIn1 = ListToVectorFilterType::New();
    // m_MaskedIn1->SetInput(outputList1);
    // m_MaskedIn1->UpdateOutputInformation();

    // Input 2
    // unsigned int nbBands2 = m_Rescaler2->GetOutput()->GetNumberOfComponentsPerPixel();
    // // sug: lire metadata depuis input1
    // itk::MetaDataDictionary &dict2 = m_Rescaler2->GetOutput()->GetMetaDataDictionary();
    // std::vector<bool> flags2;
    // std::vector<double> values2;
    // bool ret2 = otb::ReadNoDataFlags(dict2, flags2, values2);
    // if(!ret2)
    //   {
    // 	flags2.resize(nbBands2, true);
    // 	values2.resize(nbBands2, GetParameterFloat("bv"));
    //   }
    // m_V2L2 = VectorToListFilterType::New();
    // m_V2L2->SetInput(m_Rescaler2->GetOutput());
    // ImageListType::Pointer inputList2 = m_V2L2->GetOutput();
    // inputList2->UpdateOutputInformation();
    // ImageListType::Pointer outputList2 = ImageListType::New();
    // for(unsigned int i=0; i < nbBands2; ++i)
    //   {
    // 	if (flags2[i])
    // 	  {
    // 	    MaskFilterType::Pointer masker = MaskFilterType::New();
    // 	    masker->SetInput(inputList2->GetNthElement(i));
    // 	    masker->SetMaskImage(maskPtr);
    // 	    masker->SetMaskingValue(GetParameterFloat("mask.bin.nodata"));
    // 	    masker->SetOutsideValue(values2[i]);
    // 	    outputList2->PushBack(masker->GetOutput());
    // 	    m_MaskFilters2.push_back(masker);
    // 	  }
    // 	else
    // 	  {
    // 	    outputList2->PushBack(inputList2->GetNthElement(i));
    // 	  }
    //   }
    // m_MaskedIn2 = ListToVectorFilterType::New();
    // m_MaskedIn2->SetInput(outputList2);
    // m_MaskedIn2->UpdateOutputInformation();
    m_ChangeFilter->SetInput1(m_Rescaler->GetOutput());
    m_ChangeFilter->SetInput2(m_Rescaler2->GetOutput());
    if (HasValue("mask.bin.in"))
      {
	otbAppLogINFO("Using mask");
	m_ChangeFilter->SetMask(GetParameterImage<UInt8ImageType>("mask.bin.in"));
	m_ChangeFilter->SetMaskUserDefined(true);
      }
    otbAppLogINFO("MAD Estimation");
    m_ChangeFilter->GetOutput()->UpdateOutputInformation();
    m_ComputeProba->SetRho(m_ChangeFilter->GetRho());
    otbAppLogINFO("Rho:" << m_ChangeFilter->GetRho());
    // Use the normalized images to compute MAD without no data
    //m_ChangeFilter->SetInput1(m_Rescaler->GetOutput());
    //m_ChangeFilter->SetInput2(m_Rescaler2->GetOutput());//refaire
    otbAppLogINFO("rho" << m_ChangeFilter->GetRho());
    otbAppLogINFO("Compute Proba");
    
    m_ComputeProba->SetInput(m_ChangeFilter->GetOutput());
    

    SetParameterOutputImage("out", m_ComputeProba->GetOutput());
  }
  ChangeFilterType::Pointer m_ChangeFilter;
  RescalerType::Pointer m_Rescaler;
  RescalerType::Pointer m_Rescaler2;

  std::vector<MaskFilterType::Pointer> m_MaskFilters1;
  std::vector<MaskFilterType::Pointer> m_MaskFilters2;
  VectorToListFilterType::Pointer m_V2L1;
  VectorToListFilterType::Pointer m_V2L2;
  ListToVectorFilterType::Pointer m_L2V;
  ListToVectorFilterType::Pointer m_out;
  ChangeInfoFilterType::Pointer m_MetaDataChanger;
  ListToVectorFilterType::Pointer m_MaskedIn1;
  ListToVectorFilterType::Pointer m_MaskedIn2;
  
  ComputeProbaFilterType::Pointer m_ComputeProba;
}; //end of class
} // end namespace wrapper
}//end namespace otb
OTB_APPLICATION_EXPORT(otb::Wrapper::ComputeChangeProbabilities)
