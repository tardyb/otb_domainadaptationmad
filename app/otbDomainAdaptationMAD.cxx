#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "otbDomainAdaptationMAD.h"
#include "otbMaskImageFilter.h"
#include "otbMultivariateAlterationDetectorImageFilter.h"
#include "otbStatisticsXMLFileReader.h"
#include "otbShiftScaleVectorImageFilter.h"
#include "itkMaskImageFilter.h"

#include "otbImageToNoDataMaskFilter.h"
#include "otbChangeNoDataValueFilter.h"
#include "otbVectorImageToImageListFilter.h"
#include "otbImageListToVectorImageFilter.h"
#include "otbChangeInformationImageFilter.h"

#include "otbComputeChangeProbabilitiesFilter.h"
namespace otb
{
namespace Wrapper
{
class DomainAdaptationMAD : public otb::Wrapper::Application
{
public:
  typedef DomainAdaptationMAD Self;
  typedef itk::SmartPointer<Self> Pointer;

  itkNewMacro(Self);
  itkTypeMacro(DomainAdaptationMAD, otb::Wrapper::Application);


  typedef otb::ImageList<FloatImageType> ImageListType;
  typedef itk::VariableLengthVector<FloatVectorImageType::InternalPixelType>                   MeasurementType;
  typedef otb::StatisticsXMLFileReader<MeasurementType>                                        StatisticsReader;
  typedef otb::ShiftScaleVectorImageFilter<FloatVectorImageType, FloatVectorImageType>         RescalerType;
  typedef otb::MultivariateAlterationDetectorImageFilter<FloatVectorImageType, FloatVectorImageType> ChangeFilterType;
  typedef otb::DomainAdaptationMADFilter< FloatVectorImageType, FloatVectorImageType>          DomainAdaptFilterType;
  typedef itk::MaskImageFilter<FloatImageType, UInt8ImageType, FloatImageType> MaskFilterType;
  typedef otb::VectorImageToImageListFilter<FloatVectorImageType,ImageListType> VectorToListFilterType;
  typedef otb::ImageListToVectorImageFilter<ImageListType,FloatVectorImageType> ListToVectorFilterType;
  typedef otb::ChangeInformationImageFilter<FloatVectorImageType> ChangeInfoFilterType;
  typedef otb::MaskImageFilter< FloatVectorImageType, FloatVectorImageType> MaskImageFilterType;
private:
  void DoInit()
  {
    SetName("DomainAdaptationMAD");
    SetDescription("DomainAdaptation using Multivariate Alteration Detector algorithm.");
    AddParameter(ParameterType_InputImage,  "in1", "Input Image 1");
    SetParameterDescription("in1","Multiband image of the scene before perturbations");
    AddParameter(ParameterType_InputImage,  "in2", "Input Image 2");
    SetParameterDescription("in2","Mutliband image of the scene after perturbations.");
    AddParameter(ParameterType_OutputImage, "out1", "Output Image1 after projection");
    SetParameterDescription("out1","Multiband image containing change maps. Each map will be in range [-1,1], so a floating point output type is advised.");
    AddParameter(ParameterType_OutputImage, "out2", "Output Image2 after projection");
    SetParameterDescription("out2","Multiband image containing change maps. Each map will be in range [-1,1], so a floating point output type is advised.");
    AddParameter(ParameterType_Float, "bv", "Background Value");
    SetParameterDescription( "bv", "Background value to ignore in statistics computation. In the case of input mask is provided, the no data in the masked image are set to this background value and ignored." );
    SetDefaultParameterFloat("bv",0.0);
    MandatoryOff("bv");
    
    AddParameter(ParameterType_InputFilename, "imstat1", "Statistics file");
    SetParameterDescription("imstat1", "A XML file containing mean and standard deviation to center and reduce samples before classification (produced by ComputeImagesStatistics application).");
    MandatoryOff("imstat1");
    AddParameter(ParameterType_InputFilename, "imstat2", "Statistics file");
    SetParameterDescription("imstat2", "A XML file containing mean and standard deviation to center and reduce samples before classification (produced by ComputeImagesStatistics application).");
    MandatoryOff("imstat2");
    AddParameter(ParameterType_Float, "mc", "Minimum correlation value");
    SetParameterDescription("mc", "Minimum correlation value. The number of output band is defined by the number of combinations associated to a correlation value higher than mc. Default value is 10e-3. Must be between 0 and 1.");
    SetDefaultParameterFloat("mc",0.001);
    MandatoryOff("mc");

    AddParameter(ParameterType_Choice,"mask","No-data handling mode");
    SetParameterDescription("mask","Set the different option for mask use and handling no data.");
    AddChoice("mask.bin", "use a binary mask");
    AddParameter(ParameterType_InputImage, "mask.bin.in", "Binary validity mask");
    SetParameterDescription("mask.bin.in", "");
    MandatoryOff("mask.bin.in");
    AddParameter(ParameterType_Float,"mask.bin.nodata", "The no data value in the input mask.");
    SetParameterDescription("mask.bin.nodata", "");
    SetDefaultParameterFloat("mask.bin.nodata",0.0);
    MandatoryOff("mask.bin.nodata");

    AddRAMParameter();

    //AddParameter(ParameterType_OutputImage, "mad", "Output Image1 after projection");
    //SetParameterDescription("mad","Multiband image containing change maps. Each map will be in range [-1,1], so a floating point output type is advised.");
  }

  void DoUpdateParameters()
  {
    
  }

  // void DoExecuteOLD()
  // {

  //   // Check input data Rescale?
  //   StatisticsReader::Pointer  statisticsReader = StatisticsReader::New();
  //   m_Rescaler = RescalerType::New();
  //   MeasurementType  meanMeasurementVector;
  //   MeasurementType  stddevMeasurementVector;
  //   // Normalize input image if asked
  //   if(IsParameterEnabled("imstat1")  )
  //     {
  //     otbAppLogINFO("Input image normalization activated.");
  //     // Load input image statistics
  //     statisticsReader->SetFileName(GetParameterString("imstat1"));
  //     meanMeasurementVector   = statisticsReader->GetStatisticVectorByName("mean");
  //     stddevMeasurementVector = statisticsReader->GetStatisticVectorByName("stddev");
  //     otbAppLogINFO( "mean used: " << meanMeasurementVector );
  //     otbAppLogINFO( "standard deviation used: " << stddevMeasurementVector );
  //     // Rescale vector image
  //     m_Rescaler->SetScale(stddevMeasurementVector);
  //     m_Rescaler->SetShift(meanMeasurementVector);
  //     if(HasValue( "bv"))
  // 	{
  // 	  m_Rescaler->SetIgnoreUserDefinedValue(true);
  // 	  m_Rescaler->SetBgValue(GetParameterFloat("bv"));
  // 	}
  //     m_Rescaler->SetInput(GetParameterImage("in1"));
  //     }

  //   StatisticsReader::Pointer  statisticsReader2 = StatisticsReader::New();
  //   m_Rescaler2 = RescalerType::New();
  //   MeasurementType  meanMeasurementVector2;
  //   MeasurementType  stddevMeasurementVector2;
  //   // Normalize input image if asked
  //   if(IsParameterEnabled("imstat2")  )
  //     {
  //     otbAppLogINFO("Input image normalization activated.");
  //     // Load input image statistics
  //     statisticsReader2->SetFileName(GetParameterString("imstat2"));
  //     meanMeasurementVector2   = statisticsReader2->GetStatisticVectorByName("mean");
  //     stddevMeasurementVector2 = statisticsReader2->GetStatisticVectorByName("stddev");
  //     otbAppLogINFO( "mean used: " << meanMeasurementVector2 );
  //     otbAppLogINFO( "standard deviation used: " << stddevMeasurementVector2 );
  //     // Rescale vector image
  //     m_Rescaler2->SetScale(stddevMeasurementVector2);
  //     m_Rescaler2->SetShift(meanMeasurementVector2);
  //     if(HasValue( "bv"))
  // 	{
  // 	  m_Rescaler2->SetIgnoreUserDefinedValue(true);
  // 	  m_Rescaler2->SetBgValue(GetParameterFloat("bv"));
  // 	}
  //     m_Rescaler2->SetInput(GetParameterImage("in2"));
  //     }
  //   // Estimate projection matrix with MAD filter
  //   typedef otb::MultivariateAlterationDetectorImageFilter<
  //     FloatVectorImageType,
  //     FloatVectorImageType> ChangeFilterType;

  //   ChangeFilterType::Pointer m_ChangeFilter = ChangeFilterType::New();
  //   m_ChangeFilter->SetInput1(m_Rescaler->GetOutput());
  //   m_ChangeFilter->SetInput2(m_Rescaler2->GetOutput());
  //   typedef otb::DomainAdaptationMADFilter<
  //     FloatVectorImageType,
  //     FloatVectorImageType> DomainAdaptFilterType;
  //   DomainAdaptFilterType::Pointer daFilter1 = DomainAdaptFilterType::New();
  //   DomainAdaptFilterType::Pointer daFilter2 = DomainAdaptFilterType::New();
    
  //   if( HasValue( "bv" ) )
  //     {
  //       m_ChangeFilter->SetIgnoreUserDefinedValue(true);
  //       m_ChangeFilter->SetUserIgnoredValue(GetParameterFloat("bv"));
  // 	daFilter1->SetBackgroundValue(GetParameterFloat("bv"));
  // 	daFilter2->SetBackgroundValue(GetParameterFloat("bv"));
    
  //     }
  //   m_ChangeFilter->GetOutput()->UpdateOutputInformation();
  //   otbAppLogINFO("rho : "<< m_ChangeFilter->GetRho());
    
  //   DomainAdaptFilterType::VnlVectorType rho = m_ChangeFilter->GetRho();
  //   unsigned int nbOutComp = 0;
  //   double minCorr = GetParameterFloat("mc");
  //   for (unsigned int i=0; i< rho.size(); ++i)
  //   {
  //     if(rho[i] > minCorr)
  // 	{
  // 	  nbOutComp+= 1;
  // 	}
  //   }
  //   otbAppLogINFO("Number of output components : " << nbOutComp);
  //   m_Ref = m_ChangeFilter;
  //   // Get the matrix and the means
  //   daFilter1->SetNbOutBands(nbOutComp);
  //   daFilter1->SetInput(m_Rescaler->GetOutput());
  //   otbAppLogINFO("mean 1 : " << m_ChangeFilter->GetMean1());
  //   daFilter1->SetMean(m_ChangeFilter->GetMean1());
  //   daFilter1->SetV(m_ChangeFilter->GetV1());
  //   daFilter1->SetRho(m_ChangeFilter->GetRho());
  //   m_Ref2 = daFilter1;
  //   SetParameterOutputImage("out1", daFilter1->GetOutput());
  //   // Process input 1 with mean1 and v1

  //   // Process input 2 with mean 2 and v2
  //   daFilter2->SetNbOutBands(nbOutComp);
  //   //daFilter2->SetMinCorr(GetParameterFloat("mc"));
  //   daFilter2->SetInput(m_Rescaler2->GetOutput());
  //   daFilter2->SetMean(m_ChangeFilter->GetMean2());
  //   daFilter2->SetV(m_ChangeFilter->GetV2());
  //   daFilter2->SetRho(m_ChangeFilter->GetRho());
  //   m_Ref3 = daFilter2;
  //   SetParameterOutputImage("out2", daFilter2->GetOutput());
  //   // Write outputs
  // }
  void DoExecuteComplexe()
  {
    // Filter declaration
    StatisticsReader::Pointer  statisticsReader = StatisticsReader::New();
    StatisticsReader::Pointer  statisticsReader2 = StatisticsReader::New();
    m_Rescaler = RescalerType::New();
    m_Rescaler2 = RescalerType::New();

    m_ChangeFilter = ChangeFilterType::New();
    
    m_DaFilter1 = DomainAdaptFilterType::New();
    m_DaFilter2 = DomainAdaptFilterType::New();
   
    m_MaskFilter1 = MaskImageFilterType::New();
    m_MaskFilter2 = MaskImageFilterType::New();
    // usefull variables for log 
    MeasurementType  meanMeasurementVector;
    MeasurementType  stddevMeasurementVector;
    MeasurementType  meanMeasurementVector2;
    MeasurementType  stddevMeasurementVector2;

    // Define MASK inputs
    // Check if background value
    if(HasValue("bv"))
      {
	// Normalisation with background value
	m_Rescaler->SetIgnoreUserDefinedValue(true);
	m_Rescaler->SetUserIgnoredValue(GetParameterFloat("bv"));
	m_Rescaler2->SetIgnoreUserDefinedValue(true);
	m_Rescaler2->SetUserIgnoredValue(GetParameterFloat("bv"));
	// Mask filter
	m_MaskFilter1->SetBackgroundValue(GetParameterFloat("bv"));
	m_MaskFilter2->SetBackgroundValue(GetParameterFloat("bv"));
	// MAD filter
	m_ChangeFilter->SetIgnoreUserDefinedValue(true);
        m_ChangeFilter->SetUserIgnoredValue(GetParameterFloat("bv"));
	// Projection filter
	m_DaFilter1->SetBackgroundValue(GetParameterFloat("bv"));
	m_DaFilter2->SetBackgroundValue(GetParameterFloat("bv"));
 	  
      }

    // Normalization if enabled
    if(IsParameterEnabled("imstat1"))
      {
	otbAppLogINFO("Input image normalization activated for in1.");
	// Load input image statistics
	statisticsReader->SetFileName(GetParameterString("imstat1"));
	meanMeasurementVector   = statisticsReader->GetStatisticVectorByName("mean");
	stddevMeasurementVector = statisticsReader->GetStatisticVectorByName("stddev");
	otbAppLogINFO( "mean used: " << meanMeasurementVector );
	otbAppLogINFO( "standard deviation used: " << stddevMeasurementVector );
	// Set mean and var
	m_Rescaler->SetScale(stddevMeasurementVector);
	m_Rescaler->SetShift(meanMeasurementVector);
	// Rescale
	m_Rescaler->SetInput(GetParameterImage("in1"));
	m_Rescaler->GetOutput()->UpdateOutputInformation();
	if(IsParameterEnabled("mask.bin.in"))
	  {
	    otbAppLogINFO("Apply Mask 1");
	    m_MaskFilter1->SetInput1(m_Rescaler->GetOutput());
	    m_MaskFilter1->SetInput2(GetParameterImage("mask.bin.in"));    
	    m_ChangeFilter->SetInput1(m_MaskFilter1->GetOutput());

	  }
	else
	  {
	    m_ChangeFilter->SetInput1(m_Rescaler->GetOutput());
	  }
	m_DaFilter1->SetInput(m_Rescaler->GetOutput());
      }
    else
      {
	if(IsParameterEnabled("mask.bin.in"))
	  {
	    m_MaskFilter1->SetInput1(GetParameterImage("in1"));
	    m_MaskFilter1->SetInput2(GetParameterImage("mask.bin.in"));
	    m_ChangeFilter->SetInput1(m_MaskFilter1->GetOutput());
	  }
	else
	  {
	    m_ChangeFilter->SetInput1(GetParameterImage("in1"));
	  }
	m_DaFilter1->SetInput(GetParameterImage("in1"));
      }
    std::cout << "masked 1 \n";
    if(IsParameterEnabled("imstat2")  )
      {
      otbAppLogINFO("Input image normalization activated for in2.");
      // Load input image statistics
      statisticsReader2->SetFileName(GetParameterString("imstat2"));
      meanMeasurementVector2   = statisticsReader2->GetStatisticVectorByName("mean");
      stddevMeasurementVector2 = statisticsReader2->GetStatisticVectorByName("stddev");
      otbAppLogINFO( "mean used: " << meanMeasurementVector2 );
      otbAppLogINFO( "standard deviation used: " << stddevMeasurementVector2 );
      // Set mean and var
      m_Rescaler2->SetScale(stddevMeasurementVector2);
      m_Rescaler2->SetShift(meanMeasurementVector2);
      // Rescale
      m_Rescaler2->SetInput(GetParameterImage("in2"));
      m_Rescaler2->GetOutput()->UpdateOutputInformation();
      if(IsParameterEnabled("mask.bin.in"))
	{
	  otbAppLogINFO("Apply Mask 1");
	  m_MaskFilter2->SetInput1(m_Rescaler->GetOutput());
	  m_MaskFilter2->SetInput2(GetParameterImage("mask.bin.in"));
	  m_ChangeFilter->SetInput2(m_MaskFilter2->GetOutput());
	}
      else
	{
	  m_ChangeFilter->SetInput2(m_Rescaler2->GetOutput());
	}
      m_DaFilter2->SetInput(m_Rescaler2->GetOutput());
    
      }
    else
      {
	if(IsParameterEnabled("mask.bin.in"))
	  {
	    m_MaskFilter2->SetInput1(GetParameterImage("in2"));
	    m_MaskFilter2->SetInput2(GetParameterImage("mask.bin.in"));
	    m_ChangeFilter->SetInput1(m_MaskFilter2->GetOutput());
	  }
	else
	  {
	    m_ChangeFilter->SetInput1(GetParameterImage("in2"));
	  }
	m_DaFilter2->SetInput(GetParameterImage("in2"));
    
      }
    
    SetParameterOutputImage("out1", m_MaskFilter1->GetOutput());
    SetParameterOutputImage("out2", m_MaskFilter2->GetOutput());
    // // Estimate the MAD projection
    // otbAppLogINFO("MAD Estimation");
    // m_ChangeFilter->GetOutput()->UpdateOutputInformation();
    // DomainAdaptFilterType::VnlVectorType rho = m_ChangeFilter->GetRho();
    // otbAppLogINFO("rho : "<< rho);
    // unsigned int nbOutComp = 0;
    // double minCorr = GetParameterFloat("mc");
    // otbAppLogINFO("Minimum Correlation considered : " << minCorr);
    // for (unsigned int i=0; i< rho.size(); ++i)
    // {
    //   if(rho[i] > minCorr)
    // 	{
    // 	  nbOutComp+= 1;
    // 	}
    // }
    // otbAppLogINFO("Number of output components : " << nbOutComp);
    
    // // Project in1
    // m_DaFilter1->SetNbOutBands(nbOutComp);
    // m_DaFilter1->SetMean(m_ChangeFilter->GetMean1());
    // m_DaFilter1->SetV(m_ChangeFilter->GetV1());
    // m_DaFilter1->SetRho(m_ChangeFilter->GetRho());
    
    // SetParameterOutputImage("out1", m_DaFilter1->GetOutput());
  
    // // Project in2
    // m_DaFilter2->SetNbOutBands(nbOutComp);
    // m_DaFilter2->SetMean(m_ChangeFilter->GetMean2());
    // m_DaFilter2->SetV(m_ChangeFilter->GetV2());
    // m_DaFilter2->SetRho(m_ChangeFilter->GetRho());
    // m_DaFilter2->GetOutput()->UpdateOutputInformation();
    // SetParameterOutputImage("out2", m_DaFilter2->GetOutput());
    //GetParameterFloat("mask.bin.nodata")
  }

  // ListToVectorFilterType::Pointer applyMask(FloatVectorImageType::Pointer inputPtr, UInt8ImageType::Pointer maskPtr, float backgroundValue )
  // {
  //   // Test mask
  //   //FloatVectorImageType::Pointer inputPtr = m_DaFilter2->GetOutput();
  //   //UInt8ImageType::Pointer maskPtr = GetParameterImage<UInt8ImageType>("mask.bin.in");
  //   //m_MaskFilters.clear();
  //   unsigned int nbBands = inputPtr->GetNumberOfComponentsPerPixel();
  //   //std::cout << "nb bands " << nbBands << std::endl;
  //   itk::MetaDataDictionary &dict = inputPtr->GetMetaDataDictionary();
  //   std::vector<bool> flags;
  //   std::vector<double> values;
  //   bool ret = otb::ReadNoDataFlags(dict,flags,values);
  //   if (!ret)
  //     {
  //       flags.resize(nbBands,true);
  //   	// The no data value required in the output
  //       values.resize(nbBands,GetParameterFloat("bv"));
  //     }
  //   m_V2L = VectorToListFilterType::New();
  //   m_V2L->SetInput(inputPtr);
  //   ImageListType::Pointer inputList = m_V2L->GetOutput();
  //   inputList->UpdateOutputInformation();
  //   ImageListType::Pointer outputList = ImageListType::New();
  //   for (unsigned int i=0 ; i<nbBands ; ++i)
  //     {
  //   	//std::cout << "bande : " << i <<std::endl;
  //       if (flags[i])
  //         {
  //   	    MaskFilterType::Pointer masker = MaskFilterType::New();
  //   	    masker->SetInput(inputList->GetNthElement(i));
  //   	    masker->SetMaskImage(maskPtr);
  //   	    masker->SetMaskingValue(backgroundValue);
  //   	    masker->SetOutsideValue(values[i]);
  //   	    outputList->PushBack(masker->GetOutput());
  //   	    m_MaskFilters.push_back(masker);
  //         }
  //       else
  //         {
  //         outputList->PushBack(inputList->GetNthElement(i));
  //         }
  //     }
  //   m_L2V = ListToVectorFilterType::New();
  //   m_L2V->SetInput(outputList);
  //   if (!ret)
  //     {
  //       m_MetaDataChanger = ChangeInfoFilterType::New();
  //       m_MetaDataChanger->SetInput(m_L2V->GetOutput());
  //       m_MetaDataChanger->SetOutputMetaData<std::vector<bool> >(otb::MetaDataKey::NoDataValueAvailable,&flags);
  //       m_MetaDataChanger->SetOutputMetaData<std::vector<double> >(otb::MetaDataKey::NoDataValue,&values);
  //       //SetParameterOutputImage("out",m_MetaDataChanger->GetOutput());
  // 	return m_MetaDataChanger;
  //     }
  //   else
  //     {
  // 	//SetParameterOutputImage("out",m_L2V->GetOutput());
  // 	return m_L2V;
  //     }
  // }
   
  void DoExecute()
  {
    /**
     * Simplified version, all parameters are mandatory
     */
    // Filter declaration
    StatisticsReader::Pointer  statisticsReader = StatisticsReader::New();
    StatisticsReader::Pointer  statisticsReader2 = StatisticsReader::New();
    m_Rescaler = RescalerType::New();
    m_Rescaler2 = RescalerType::New();

    m_ChangeFilter = ChangeFilterType::New();
    
    m_DaFilter1 = DomainAdaptFilterType::New();
    m_DaFilter2 = DomainAdaptFilterType::New();
   
    m_MaskFilter1 = MaskImageFilterType::New();
    m_MaskFilter2 = MaskImageFilterType::New();
    // usefull variables for log 
    MeasurementType  meanMeasurementVector;
    MeasurementType  stddevMeasurementVector;
    MeasurementType  meanMeasurementVector2;
    MeasurementType  stddevMeasurementVector2;

    // Set Background value
    m_Rescaler->SetIgnoreUserDefinedValue(true);
    m_Rescaler->SetUserIgnoredValue(GetParameterFloat("bv"));
    m_Rescaler2->SetIgnoreUserDefinedValue(true);
    m_Rescaler2->SetUserIgnoredValue(GetParameterFloat("bv"));

    m_ChangeFilter->SetIgnoreUserDefinedValue(true);
    m_ChangeFilter->SetUserIgnoredValue(GetParameterFloat("bv"));

    m_DaFilter1->SetBackgroundValue(GetParameterFloat("bv"));
    m_DaFilter2->SetBackgroundValue(GetParameterFloat("bv"));
 	  
    
    // Normalisation

    // Input 1
    otbAppLogINFO("Input image normalization activated for in1.");
    // Load input image statistics
    statisticsReader->SetFileName(GetParameterString("imstat1"));
    meanMeasurementVector   = statisticsReader->GetStatisticVectorByName("mean");
    stddevMeasurementVector = statisticsReader->GetStatisticVectorByName("stddev");
    otbAppLogINFO( "mean used: " << meanMeasurementVector );
    otbAppLogINFO( "standard deviation used: " << stddevMeasurementVector );
    // Set mean and var
    m_Rescaler->SetScale(stddevMeasurementVector);
    m_Rescaler->SetShift(meanMeasurementVector);
    // Rescale
    m_Rescaler->SetInput(GetParameterImage("in1"));
    m_Rescaler->GetOutput()->UpdateOutputInformation();

    // Input 2
    otbAppLogINFO("Input image normalization activated for in2.");
    // Load input image statistics
    statisticsReader2->SetFileName(GetParameterString("imstat2"));
    meanMeasurementVector2   = statisticsReader2->GetStatisticVectorByName("mean");
    stddevMeasurementVector2 = statisticsReader2->GetStatisticVectorByName("stddev");
    otbAppLogINFO( "mean used: " << meanMeasurementVector2 );
    otbAppLogINFO( "standard deviation used: " << stddevMeasurementVector2 );
    // Set mean and var
    m_Rescaler2->SetScale(stddevMeasurementVector2);
    m_Rescaler2->SetShift(meanMeasurementVector2);
    // Rescale
    m_Rescaler2->SetInput(GetParameterImage("in2"));
    m_Rescaler2->GetOutput()->UpdateOutputInformation();

    // Apply Mask 
    UInt8ImageType::Pointer maskPtr = GetParameterImage<UInt8ImageType>("mask.bin.in");

    // Input 1
    unsigned int nbBands1 = m_Rescaler->GetOutput()->GetNumberOfComponentsPerPixel();
    // sug: lire metadata depuis input1
    itk::MetaDataDictionary &dict1 = m_Rescaler->GetOutput()->GetMetaDataDictionary();
    std::vector<bool> flags1;
    std::vector<double> values1;
    bool ret1 = otb::ReadNoDataFlags(dict1, flags1, values1);
    if(!ret1)
      {
	flags1.resize(nbBands1, true);
	values1.resize(nbBands1, GetParameterFloat("bv"));
      }
    m_V2L1 = VectorToListFilterType::New();
    m_V2L1->SetInput(m_Rescaler->GetOutput());
    ImageListType::Pointer inputList1 = m_V2L1->GetOutput();
    inputList1->UpdateOutputInformation();
    ImageListType::Pointer outputList1 = ImageListType::New();
    for(unsigned int i=0; i < nbBands1; ++i)
      {
	if (flags1[i])
	  {
	    MaskFilterType::Pointer masker = MaskFilterType::New();
	    masker->SetInput(inputList1->GetNthElement(i));
	    masker->SetMaskImage(maskPtr);
	    masker->SetMaskingValue(GetParameterFloat("mask.bin.nodata"));
	    masker->SetOutsideValue(values1[i]);
	    outputList1->PushBack(masker->GetOutput());
	    m_MaskFilters1.push_back(masker);
	  }
	else
	  {
	    outputList1->PushBack(inputList1->GetNthElement(i));
	  }
      }
    m_MaskedIn1 = ListToVectorFilterType::New();
    m_MaskedIn1->SetInput(outputList1);
    m_MaskedIn1->UpdateOutputInformation();

    // Input 2
    unsigned int nbBands2 = m_Rescaler2->GetOutput()->GetNumberOfComponentsPerPixel();
    // sug: lire metadata depuis input1
    itk::MetaDataDictionary &dict2 = m_Rescaler2->GetOutput()->GetMetaDataDictionary();
    std::vector<bool> flags2;
    std::vector<double> values2;
    bool ret2 = otb::ReadNoDataFlags(dict2, flags2, values2);
    if(!ret2)
      {
	flags2.resize(nbBands2, true);
	values2.resize(nbBands2, GetParameterFloat("bv"));
      }
    m_V2L2 = VectorToListFilterType::New();
    m_V2L2->SetInput(m_Rescaler2->GetOutput());
    ImageListType::Pointer inputList2 = m_V2L2->GetOutput();
    inputList2->UpdateOutputInformation();
    ImageListType::Pointer outputList2 = ImageListType::New();
    for(unsigned int i=0; i < nbBands2; ++i)
      {
	if (flags2[i])
	  {
	    MaskFilterType::Pointer masker = MaskFilterType::New();
	    masker->SetInput(inputList2->GetNthElement(i));
	    masker->SetMaskImage(maskPtr);
	    masker->SetMaskingValue(GetParameterFloat("mask.bin.nodata"));
	    masker->SetOutsideValue(values2[i]);
	    outputList2->PushBack(masker->GetOutput());
	    m_MaskFilters2.push_back(masker);
	  }
	else
	  {
	    outputList2->PushBack(inputList2->GetNthElement(i));
	  }
      }
    m_MaskedIn2 = ListToVectorFilterType::New();
    m_MaskedIn2->SetInput(outputList2);
    m_MaskedIn2->UpdateOutputInformation();

    
    // Compute MAD
    if (nbBands1 == nbBands2)
      {
	std::cout << "Same number of output bands\n";
	m_DaFilter1->SetSameNumberOfBands(true);
	m_DaFilter2->SetSameNumberOfBands(true);
      }
    else
      {
	m_DaFilter1->SetSameNumberOfBands(false);
	m_DaFilter2->SetSameNumberOfBands(false);
      }
    
    m_ChangeFilter->SetInput1(m_MaskedIn1->GetOutput());
    m_ChangeFilter->SetInput2(m_MaskedIn2->GetOutput());
    otbAppLogINFO("MAD Estimation");
    m_ChangeFilter->GetOutput()->UpdateOutputInformation();
    DomainAdaptFilterType::VnlVectorType rho = m_ChangeFilter->GetRho();
    otbAppLogINFO("Rho : " << rho);
    unsigned int nbOutComp = 0;
    double minCorr = GetParameterFloat("mc");
    otbAppLogINFO("Minimum Correlation considered : " << minCorr);
    for (unsigned int i=0; i< rho.size(); ++i)
    {
      if(rho[i] > minCorr)
    	{
    	  nbOutComp+= 1;
    	}
    }
    otbAppLogINFO("Number of output components : " << nbOutComp);
    
    
    // Project normalised inputs
    m_DaFilter1->SetNbOutBands(nbOutComp);
    m_DaFilter1->SetMean(m_ChangeFilter->GetMean1());
    m_DaFilter1->SetV(m_ChangeFilter->GetV1());
    m_DaFilter1->SetInput(m_Rescaler->GetOutput());

    m_DaFilter2->SetNbOutBands(nbOutComp);
    m_DaFilter2->SetMean(m_ChangeFilter->GetMean2());
    m_DaFilter2->SetV(m_ChangeFilter->GetV2());
    m_DaFilter2->SetInput(m_Rescaler2->GetOutput());
    
    // Write output
    SetParameterOutputImage("out1", m_DaFilter1->GetOutput());
    SetParameterOutputImage("out2", m_DaFilter2->GetOutput());
    //otbAppLogINFO("Ecrit maskedIn");
    //SetParameterOutputImage("out1", m_MaskedIn1->GetOutput());
    //SetParameterOutputImage("out2", m_MaskedIn2->GetOutput());
    //SetParameterOutputImage("mad", m_ChangeFilter->GetOutput());
  }

  ChangeFilterType::Pointer m_ChangeFilter;
  DomainAdaptFilterType::Pointer m_DaFilter1;
  DomainAdaptFilterType::Pointer m_DaFilter2;
  MaskImageFilterType::Pointer m_MaskFilter1;
  MaskImageFilterType::Pointer m_MaskFilter2;
  RescalerType::Pointer m_Rescaler;
  RescalerType::Pointer m_Rescaler2;
  std::vector<MaskFilterType::Pointer> m_MaskFilters1;
  std::vector<MaskFilterType::Pointer> m_MaskFilters2;
  VectorToListFilterType::Pointer m_V2L1;
  VectorToListFilterType::Pointer m_V2L2;
  ListToVectorFilterType::Pointer m_L2V;
  ListToVectorFilterType::Pointer m_out;
  ChangeInfoFilterType::Pointer m_MetaDataChanger;
  ListToVectorFilterType::Pointer m_MaskedIn1;
  ListToVectorFilterType::Pointer m_MaskedIn2;
};
} // end namespace Wrapper
} // end namespace OTB
OTB_APPLICATION_EXPORT(otb::Wrapper::DomainAdaptationMAD)
